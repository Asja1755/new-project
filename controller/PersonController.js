const models = require("../models")
const config = require("../config/app.json")


const PersonIndex = (req, res) => {
    res.render("person/index")
};

const createPerson = (req, res) => {
    res.render('person/store', {
        title: 'createPerson',
    })
}

const storePerson = (req, res) => {
    models.Person.create({
        name: req.body.name,
        email: req.body.email,
    }).then(result => {
        const personId = result.id
        res.redirect("/person/"+personId+"/lost-things")
    }).
    catch(err => {
        res.send(err)
    })
}

const lostThings = (req, res) => {
    models.Person.findOne({
        where: {
            id: req.params.personId
        }
    })
        .then(person => {
            res.render('person/show', {
                person: person
            });
        })
        .catch(error => {
            console.log("Error: " + error)
        });}


module.exports = {
    PersonIndex, createPerson, storePerson, lostThings
};