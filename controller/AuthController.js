const models = require("../models");

const index = (req, res) => {
    models.users.findByPk(req.session.userObject.id)
        .then(user => {
            res.render("adminOverview", {user:user})
        }).catch(err => {
        req.session.userObject = null;
        res.render("/login")
    })
};

const destroy = (req, res ) => {
    req.session.userObject = null;
    res.redirect('/');
};

module.exports = {
    index, destroy
};
