const models = require("../models")
const config = require("../config/app.json")


const ThingIndex = (req, res) => {
    models.Thing.findAll().then(things => {
        res.render("things/index", {
            path: "/thing",
            things: things
        })
    }).catch(err => {
        console.log(err);
        res.end()
    });
};

const showThing = (req, res) => {
    models.Thing.findOne({
        where: {
            id: req.params.thingId
        }
    })
        .then(thing => {
            res.render('things/show', {
                name: 'updateThing',
                description: 'update the conference',
                thing: thing
            });
        })
        .catch(error => {
            console.log("Error: " + error)
        });
};


const createThing = (req, res) => {
    res.render('things/store', {
        title: 'createThing',
        description: 'What kind of conference do you want to create?'
    })
}

const storeThing = (req, res) => {
    const person = req.params.personId
    models.Thing.create({
        name: req.body.name,
        description: req.body.description,
        date: req.body.date,
        personId: person,
        place: req.body.place
    })
        .then(result => {
            res.redirect('/things');
        }).
    catch(err => {
        res.send(err)
    })
}

const editThing = (req, res) => {
    models.Thing.findOne({
        where: {
            id: req.params.thingId
        }
    })
        .then(thing => {
            res.render('things/edit', {
                name: 'updateThing',
                description: 'update the conference',
                thing: thing
            });
        })
        .catch(error => {
            console.log("Error: " + error)
        });
};


const updateThing = (req, res) => {
    models.Thing.update({
        name: req.body.name,
        description: req.body.description,
        date: req.body.date,
    }, {
        where: {
            id: req.params.conferenceId
        }
    }).then(result => {
        console.log("updated successfully :" + result);
        res.redirect('/thing/show/'+ req.params.thingId);
    }).catch(error => {
        console.log("Error: " + error);
    });
};

const destroyThing = (req, res) => {
    models.Thing.findOne({
        where: {
            id: req.params.thingId
        }
    })
        .then(thing => {
            return thing.destroy();
        }).then(result => {
        console.log("successfully deleted :" + result);
        res.redirect('/things');
    })
        .catch(error => {
            console.log("Error: " + error)
        });
};



module.exports = {
    ThingIndex, showThing, createThing, storeThing, editThing, updateThing, destroyThing
}
