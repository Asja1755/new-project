const models = require("../models");
const bcrypt = require("bcrypt");
const moment = require("moment");
const config = require("../config/app.json");

const index = (req, res) => {
    models.users.findByPk(req.session.userObject)
        .then(user => {
            res.render("login")
        })
};

const auth = (req, res) => {
    console.log(req.body);
    models.users.findOne({
        where: {
            name: req.body.name
        }
    }).then(user => {
        bcrypt.compare(req.body.password, user.password, function(err, result) {
            if (result) {
                req.session.userObject ={
                    id: user.id,
                    name: user.name,
                    email: user.email,
                    date: moment(user.createdAt).format(' Do MMMM YYYY,')
                };
                res.redirect("/things");
            } else {
                res.send("Fout wachtwoord")
            }
        });
    });
};


module.exports = {
    index, auth
};
