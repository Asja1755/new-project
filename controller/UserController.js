const models = require("../models")
const bcrypt = require("bcrypt")
const config = require("../config/app.json")

const index = (req, res) => {
    models.users.findAll().then(users => {
        res.render("register", {
            users: users
        })
    }).catch(err => {
        console.log(err);
        res.end()
    });
};

const show = (req, res) => {
    models.users.findByPk(req.param('user'))
        .then(user => {
            res.render("dashboard/userdashboard", user)
        })
};


const add = (req, res) => {
    const password = req.body.password;

    bcrypt.hash(password, config.saltRounds, function(err, hash) {
        models.users.create({
            name: req.body.name,
            email: req.body.email,
            password: hash
        }).then(user => {
            res.redirect("/login",)
        }).catch(err => {
            res.send(err)
        })
    });
};



const edit = (req, res) => {
    models.users.findOne({
        id: req.body.id
    }).then(user => {
        user.email = req.body.email;
        user.save().then(r => {
            res.send("New email is set");
        })
    })
};


const adminIndex = (req, res) => {
    models.Place.findAll().then(places => {
        console.log(places)
        res.render("adminOverview", {
            places: places
        })
    }).catch(err => {
        console.log(err);
        res.end()
    });
};


const storePlace = (req, res) => {
    models.Place.create({
        name: req.body.name,
    })
        .then(place => {
            res.redirect('/admin');
        }).
    catch(err => {
        res.send(err)
    })
}

module.exports = {
    index, add, show, adminIndex, storePlace
};
