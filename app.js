const express = require("express");
const config = require("./config/app.json");
const site = require("./routes/site");
const session = require("express-session");
const bodyParser = require('body-parser');


const app = express();

const port = 9000;

const prefix = config.enviroment === "dev" ? "" : "/~anastasiya/dagtentamen/69/";

//session
app.set('trust proxy', 1);
app.use(session({
        secret: 'keyboard cat',
        resave: false,
        saveUninitialized: true,
        cookie: {secure: 'auto'}
    })
);

app.use(function (req, res, next) {
    res.locals.userObject = req.session.userObject;
    next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.set("view engine", "ejs");
app.set("views", `${process.cwd()}/resources/views`);

app.use(`${prefix}/`, site);

app.listen(port, () => {
    console.log(`Website on ${port}`);
});
