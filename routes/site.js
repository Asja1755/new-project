const express = require("express");
const controller = require("../controller/index");
const bodyParser = require("body-parser");

const router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: true
}));

router

    //auth
    .get("/register", controller.UserController.index)
    .get("/login", controller.LoginController.index)
    .get("/userdashboard", controller.AuthController.index)
    .post("/register", controller.UserController.add)
    .post("/login", controller.LoginController.auth)
    .get("/uitloggen", controller.AuthController.destroy)

    // .get("/admin_create", controller.UserController.admin_create)
    // .post("/add-admin", controller.UserController.addAdmin)
    //
    // .post("/login", controller.UserController.login)
    .get("/things", controller.ThingsController.ThingIndex)
    .get('/thing/show/:thingId', controller.ThingsController.showThing)
    .get("/thing/create", controller.ThingsController.createThing)
    .get('/thing/edit/:thingId',controller.ThingsController.editThing)
    .post('/thing/update/:thingId', controller.ThingsController.updateThing)
    .post('/thing/destroy/:thingId', controller.ThingsController.destroyThing)

    .get("/", controller.PersonController.PersonIndex)
    .get("/person/register", controller.PersonController.createPerson)
    .post("/person/register", controller.PersonController.storePerson)

    .get("/person/:personId/lost-things", controller.PersonController.lostThings)
    .post("/person/:personId/thing", controller.ThingsController.storeThing)

    .get("/admin", controller.UserController.adminIndex)
    .post("/place/store", controller.UserController.storePlace);
module.exports = router;
