'use strict';
module.exports = (sequelize, DataTypes) => {
  const Thing = sequelize.define('Thing', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    date: DataTypes.DATE,
    personId: DataTypes.INTEGER,
  }, {});
  Thing.associate = function(models) {
    // associations can be defined here
    Thing.belongsTo(models.Person, { foreignKey: 'personId'});
  };
  return Thing;
};